import os
import sys
import random

__author__ = "Кондратьев Артем 11-901"

pids = list()
arguments = int(sys.argv[1])
for i in range(arguments):
    child = os.fork()
    if child == 0:
        os.execvp("python3", ("python3", "child.py", str(random.randint(5, 10))))
    if child != 0:
        pids.append(child)

for pid in pids:
    status = os.waitpid(pid, 0)
    print(f"Process with pid", pid, " - finished. Exit status -", status)