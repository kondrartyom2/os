import os
import sys
import time
import random

string_ = "Child started with pid " + str(os.getpid()) + " and argument " + str(sys.argv[1])

os.system('echo "{}" > /dev/tty1'.format(string_))

seconds = int(sys.argv[1])
time.sleep(seconds)

os._exit(random.choice([0, 1]))